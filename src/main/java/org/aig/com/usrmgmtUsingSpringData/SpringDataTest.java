package org.aig.com.usrmgmtUsingSpringData;

import java.util.ArrayList;
import java.util.List;

import org.aig.com.usrmgmtUsingSpringData.dao.UserDao;
import org.aig.com.usrmgmtUsingSpringData.dao.UserDaoImpl;
import org.aig.com.usrmgmtUsingSpringData.entity.Address;
import org.aig.com.usrmgmtUsingSpringData.entity.User;

/**
 * @author PSingh2
 *
 */
public class SpringDataTest {

	public static void main(String[] args) {
	
		User usr = new User("Priya.Singh@mindtree.com", "Priya", 8095201764L, "Female");
		Address address1 = new Address("K-34 Siroman", "jAMSHEDPUR", "JharKhand", "INDIA", 831018);
		Address address2 = new Address("Flat No: 202", "Ranchi", "JharKhand", "INDIA", 834002);

		List<Address> addressList = new ArrayList<>();
		addressList.add(address1);
		addressList.add(address2);
		usr.setAddress(addressList);

		UserDao userDao = new UserDaoImpl();
		List<User> userList=userDao.getAllUser();
		for(User user:userList)
			System.out.println(user.getName());
}	

}
