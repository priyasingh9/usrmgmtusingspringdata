package org.aig.com.usrmgmtUsingSpringData.repositories;

import org.aig.com.usrmgmtUsingSpringData.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author PSingh2
 *
 */
public interface UserRepository extends JpaRepository<User, Integer> {

}
