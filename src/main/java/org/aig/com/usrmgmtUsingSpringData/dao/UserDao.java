package org.aig.com.usrmgmtUsingSpringData.dao;

import java.util.List;

import org.aig.com.usrmgmtUsingSpringData.entity.User;
import org.aig.com.usrmgmtUsingSpringData.exception.DataNotFoundException;

/**
 * @author PSingh2
 *
 */
public interface UserDao {

	public User deleteUser(int userId);

	public User findByPrimaryKey(int userId) throws DataNotFoundException;

	public User save(User user);

	public List<User> getAllUser();

	User updateUser(int userId,User user);

}
