package org.aig.com.usrmgmtUsingSpringData.dao;


import java.util.List;


import org.aig.com.usrmgmtUsingSpringData.entity.User;
import org.aig.com.usrmgmtUsingSpringData.exception.DataNotFoundException;
import org.aig.com.usrmgmtUsingSpringData.repositories.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author PSingh2
 *
 */

public class UserDaoImpl implements UserDao {

	final static Logger logger = Logger.getLogger(UserDaoImpl.class);
	@Autowired
	private UserRepository userRepository; 

	/**
	 * @param UserId and User Details
	 * @return Details of the user
	 */
	@Override
	public User updateUser(int userId,User user) {
		/*User userNew=new User();
		userNew.setEmailId(user.getEmailId());
		userNew.setGender(user.getGender());
		userNew.setName(user.getName());
		userNew.setPhone(user.getPhone());
	    userNew.setAddress(user.getAddress());
	    userNew.setUserId(userId);*/
		return userRepository.save(user);
	}

	/**
	 * @param UserId
	 * @return Details of the User
	 */
	@Override
	public User deleteUser(int userId) {
		User user = userRepository.findOne(userId);
		userRepository.delete(user);
		logger.info("User Id "+userId+" is Deleted");
		return user;
	}

	/**
	 * @param UserId
	 * @return Details of the User
	 */
	@SuppressWarnings("unused")
	@Override
	public User findByPrimaryKey(int userId) throws DataNotFoundException {
		User user = userRepository.findOne(userId);
		System.out.println("User Details "+user.getEmailId()+"User Details: "+user.getName());
		if(user==null){
			logger.info("User Data Not Found");
			throw new DataNotFoundException("User Data Not Found");
		}
		return user;
	}

	/**
	 * @param Details of the User
	 * @return Details of the User
	 */
	@Override
	public User save(User user) {
		logger.info("New User has been Added");
		return userRepository.save(user);
	}

	/**
	 * @return List of all Users
	 */
	@Override
	public List<User> getAllUser() {
		return  userRepository.findAll();
	}

}
