package org.aig.com.usrmgmtUsingSpringData.utilities;

import java.util.ArrayList;
import java.util.Collection;

import org.aig.com.usrmgmtUsingSpringData.model.Address;
import org.aig.com.usrmgmtUsingSpringData.model.User;

/**
 * @author PSingh2
 *
 */
public class DTOtoModelMapper {

	/**
	 * @param userModel
	 * @return userEntity
	 */
	public static org.aig.com.usrmgmtUsingSpringData.entity.User modelToDto(User userModel) {
		org.aig.com.usrmgmtUsingSpringData.entity.User userDto = new org.aig.com.usrmgmtUsingSpringData.entity.User();
		userDto.setUserId(userModel.getUserId());
		userDto.setName(userModel.getName());
		userDto.setGender(userModel.getGender());
		userDto.setPhone(userModel.getPhone());
		userDto.setEmailId(userModel.getEmailId());
		Collection<Address> addressListModel = userModel.getAddress();
		Collection<org.aig.com.usrmgmtUsingSpringData.entity.Address> addressListDto = new ArrayList<>();
		org.aig.com.usrmgmtUsingSpringData.entity.Address addressDto = new org.aig.com.usrmgmtUsingSpringData.entity.Address();
		for (Address addressModel : addressListModel) {
			addressDto = modelToDtoAddress(addressModel);
			addressListDto.add(addressDto);
		}
		userDto.setAddress(addressListDto);
		return userDto;
	}

	/**
	 * @param userEntity
	 * @return userModel
	 */
	public static User dtoToModel(org.aig.com.usrmgmtUsingSpringData.entity.User userDto) {
		User userModel = new User();
		userModel.setUserId(userDto.getUserId());
		userModel.setName(userDto.getName());
		userModel.setGender(userDto.getGender());
		userModel.setPhone(userDto.getPhone());
		userModel.setEmailId(userDto.getEmailId());
		Collection<org.aig.com.usrmgmtUsingSpringData.entity.Address> addressListDto = userDto.getAddress();
		Collection<Address> addressListModel = new ArrayList<>();
		Address addressModel = new Address();
		for (org.aig.com.usrmgmtUsingSpringData.entity.Address addressDto : addressListDto) {
			addressModel = dtoToModelAddress(addressDto);
			addressListModel.add(addressModel);
		}
		userModel.setAddress(addressListModel);
        return userModel;
	}

	/**
	 * @param addressEntity
	 * @return addressModel
	 */
	public static Address dtoToModelAddress(org.aig.com.usrmgmtUsingSpringData.entity.Address addressDto) {
		Address addressModel = new Address();
		addressModel.setAddressId(addressDto.getAddressId());
		addressModel.setCity(addressDto.getCity());
		addressModel.setCountry(addressDto.getCountry());
		addressModel.setState(addressDto.getState());
		addressModel.setPin(addressDto.getPin());
		addressModel.setStreet(addressDto.getStreet());

		return addressModel;
	}

	/**
	 * @param addressModel
	 * @return addressEntity
	 */
	public static org.aig.com.usrmgmtUsingSpringData.entity.Address modelToDtoAddress(Address addressModel) {
		org.aig.com.usrmgmtUsingSpringData.entity.Address addressDto = new org.aig.com.usrmgmtUsingSpringData.entity.Address();
		addressDto.setAddressId(addressModel.getAddressId());
		addressDto.setCity(addressModel.getCity());
		addressDto.setCountry(addressModel.getCountry());
		addressDto.setPin(addressModel.getPin());
		addressDto.setState(addressModel.getState());
		addressDto.setStreet(addressModel.getStreet());
		return addressDto;
	}
}
