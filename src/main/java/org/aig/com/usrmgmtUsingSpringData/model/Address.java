package org.aig.com.usrmgmtUsingSpringData.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class Address {

	private int addressId;

	private String street;

	private String city;

	private String state;

	private String country;

	private int pin;
	
	public Address(){
		
	}

	public Address(String street, String city, String state, String country, int pin) {
		super();
		this.street = street;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pin = pin;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

}
