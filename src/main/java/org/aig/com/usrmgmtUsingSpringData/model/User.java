package org.aig.com.usrmgmtUsingSpringData.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import org.aig.com.usrmgmtUsingSpringData.model.Address;

@XmlRootElement

public class User {

	private Integer userId;

	private String emailId;

	private String name;

	private long phone;

	private String gender;

	private Collection<Address> address = new ArrayList<Address>();
	
	public User(){
		
	}

	public User( String emailId, String name, long phone, String gender) {
		super();
		
		this.emailId = emailId;
		this.name = name;
		this.phone = phone;
		this.gender = gender;
		
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Collection<Address> getAddress() {
		return address;
	}

	public void setAddress(Collection<org.aig.com.usrmgmtUsingSpringData.model.Address> collection) {
		this.address = collection;
	}
	

}
