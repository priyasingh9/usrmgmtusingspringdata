package org.aig.com.usrmgmtUsingSpringData.exception;

/**
 * @author PSingh2
 *
 */
public class DataNotFoundException extends Exception {

	/**
	 * DataNotFoundException Defined 
	 */
	private static final long serialVersionUID = 1L;
	
	public DataNotFoundException(String msg){
		super(msg);
	}
	

}
