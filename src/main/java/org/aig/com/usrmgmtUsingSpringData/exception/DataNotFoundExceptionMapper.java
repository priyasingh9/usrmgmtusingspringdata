package org.aig.com.usrmgmtUsingSpringData.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.aig.com.usrmgmtUsingSpringData.entity.ErrorMessage;

/**
 * @author PSingh2
 *
 */
public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException>{

	/**
	 * @return Response for DataNotFoundException
	 */
	@Override
	public Response toResponse(DataNotFoundException ex) {
		ErrorMessage errormessage=new ErrorMessage(ex.getMessage(),404,"www.google.com");
		Response response=Response.status(Status.NOT_FOUND).entity(errormessage).build();
		return response;
	}

}
