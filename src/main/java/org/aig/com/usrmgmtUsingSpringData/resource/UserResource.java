package org.aig.com.usrmgmtUsingSpringData.resource;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.aig.com.usrmgmtUsingSpringData.model.User;
import org.aig.com.usrmgmtUsingSpringData.exception.DataNotFoundException;
import org.aig.com.usrmgmtUsingSpringData.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author PSingh2
 *
 */
@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {
	
	@Autowired
	private UserService userService;

	/**
	 * @return List of the user
	 */
	@GET
	public List<User> getAllUser() {
		List<User> userList = userService.getAllUser();
		return userList;
	}

	/**
	 * @param userId
	 * @return user Details
	 * @throws DataNotFoundException
	 */
	@GET
	@Path("/{userId}")
	public User getUser(@PathParam("userId") int userId) throws DataNotFoundException {
		User user=userService.findByPrimaryKey(userId);
		return  user;

	}

	/**
	 * @param user Details
	 * @return user Details
	 */
	@POST
	public User addUser(@NotNull User user) {
		return userService.save(user);

	}

	/**
	 * @param user Details
	 * @return user Details
	 */
	@PUT
	public User updateUser(User user) {
		int userId=user.getUserId();
		return userService.updateUser(userId, user);

	}

	/**
	 * @param userId
	 * @return message
	 */
	@DELETE
	@Path("/{userId}")
	public String deleteUser(@PathParam("userId") int userId) {
		userService.deleteUser(userId);
		return "UserId " + userId + " is deleted";
	}
}
