package org.aig.com.usrmgmtUsingSpringData.service;

import java.util.List;

import org.aig.com.usrmgmtUsingSpringData.model.User;
import org.aig.com.usrmgmtUsingSpringData.exception.DataNotFoundException;

/**
 * @author PSingh2
 *
 */
public interface UserService {
	public User deleteUser(int userId);

	public User findByPrimaryKey(int userId) throws DataNotFoundException;

	public User save(User user);

	public List<User> getAllUser();

	User updateUser(Integer userId, User user);
}
