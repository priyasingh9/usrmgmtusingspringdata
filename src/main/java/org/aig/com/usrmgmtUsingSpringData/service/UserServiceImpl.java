package org.aig.com.usrmgmtUsingSpringData.service;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.aig.com.usrmgmtUsingSpringData.dao.UserDao;
import org.aig.com.usrmgmtUsingSpringData.exception.DataNotFoundException;
import org.aig.com.usrmgmtUsingSpringData.model.User;
import org.aig.com.usrmgmtUsingSpringData.utilities.DTOtoModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author PSingh2
 *
 */
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * @param userId
	 * @return User Details
	 */
	@Override
	public User deleteUser(int userId) {
		org.aig.com.usrmgmtUsingSpringData.entity.User userModel = userDao.deleteUser(userId);
		return DTOtoModelMapper.dtoToModel(userModel);
	}

	/**
	 * @param userId
	 * @return User Details
	 * @throws DataNotFoundException
	 */
	@Override
	public User findByPrimaryKey(int userId) throws DataNotFoundException {
		org.aig.com.usrmgmtUsingSpringData.entity.User userModel = userDao.findByPrimaryKey(userId);
		return DTOtoModelMapper.dtoToModel(userModel);
	}

	/**
	 * @param user
	 *            Details
	 * @return User Details
	 */
	@Override
	public User save(User user) {
		org.aig.com.usrmgmtUsingSpringData.entity.User modelUser = DTOtoModelMapper.modelToDto(user);
		org.aig.com.usrmgmtUsingSpringData.entity.User usr = userDao.save(modelUser);
		return DTOtoModelMapper.dtoToModel(usr);
	}

	/**
	 * @return List of all Users Details
	 */
	@Override
	public List<User> getAllUser() {
		List<org.aig.com.usrmgmtUsingSpringData.entity.User> userModelList = userDao.getAllUser();

		List<User> usersListInDTO = convertEntiyListToDtoList(userModelList);
		return usersListInDTO;
	}

	/**
	 * This Convert the List of
	 * {@link org.aig.com.usrmgmtUsingSpringData.entity.User} into List of
	 * {@link User} DTO.
	 * 
	 * @param userModelList
	 * @return Collection of {@link User}
	 */
	private List<User> convertEntiyListToDtoList(List<org.aig.com.usrmgmtUsingSpringData.entity.User> userModelList) {
		if (userModelList == null || userModelList.isEmpty()) {
			return null;
		}
		ListIterator<org.aig.com.usrmgmtUsingSpringData.entity.User> userModeListIterator = userModelList
				.listIterator();
		List<User> userDtoList = new ArrayList<User>();
		while (userModeListIterator.hasNext()) {
			userDtoList.add(DTOtoModelMapper.dtoToModel(userModeListIterator.next()));
		}
       return userDtoList;
	}

	/**
	 * @param userId
	 *            and User Details
	 * @return User Details
	 */
	@Override
	public User updateUser(Integer userId, User user) {
		org.aig.com.usrmgmtUsingSpringData.entity.User modelUser = DTOtoModelMapper.modelToDto(user);
		return DTOtoModelMapper.dtoToModel(userDao.updateUser(userId, modelUser));
	}

}
